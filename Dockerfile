FROM maven:3.9.8-amazoncorretto-21-debian AS builder

ADD ./pom.xml pom.xml
ADD ./src src/

RUN ldd --version
RUN mvn clean package -Pproduction

From eclipse-temurin:21.0.4_7-jre-noble

COPY --from=builder target/vaadin-0.0.1-SNAPSHOT.jar vaadin-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java", "-jar", "vaadin-0.0.1-SNAPSHOT.jar"]