package de.firetag.vaadin.health;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/healthz")
public class HealthController {
    @GetMapping
    public ResponseEntity<Boolean> getHealth() {
        return ResponseEntity.ok(true);
    }

}
