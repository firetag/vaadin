package de.firetag.vaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FiretagVaadinUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FiretagVaadinUiApplication.class, args);
	}

}
