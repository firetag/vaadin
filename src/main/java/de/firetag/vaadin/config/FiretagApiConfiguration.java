package de.firetag.vaadin.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "firetag.service")
@Getter
@Setter
public class FiretagApiConfiguration {
    @Value("url")
    private String url;
}
