package de.firetag.vaadin.views.main.holiday.next.component;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.event.ApiRequestDataChangedEventPublisher;
import de.firetag.vaadin.views.main.holiday.component.StateSelection;

@SpringComponent
@UIScope
public class NextStateSelection extends StateSelection {

    public NextStateSelection(final ApiRequestDataChangedEventPublisher publisher) {
        super(publisher);
        this.stateSelect.setValue(null);
    }
}
