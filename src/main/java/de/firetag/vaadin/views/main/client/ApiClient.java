package de.firetag.vaadin.views.main.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.firetag.vaadin.config.FiretagApiConfiguration;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.client.model.HolidayDTO;
import de.firetag.vaadin.views.main.holiday.model.state.State;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;

@Service
public class ApiClient {

    private final FiretagApiConfiguration firetagApiConfiguration;
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;

    public ApiClient(final FiretagApiConfiguration firetagApiConfiguration,
                     final HttpClient httpClient,
                     final ObjectMapper objectMapper) {
        this.firetagApiConfiguration = firetagApiConfiguration;
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }

    public HolidayDTO fetchHoliday(final ApiRequestData apiRequestData) {
        if (apiRequestData == null) {
            return null;
        }
        if (apiRequestData.getDate() == null) {
            return fetchNextHoliday(apiRequestData);
        } else {
            return fetchDatedHoliday(apiRequestData);
        }

    }

    private HolidayDTO fetchDatedHoliday(@NonNull final ApiRequestData apiRequestData) {
        if (apiRequestData.getDate() == null) {
            return null;
        }
        return sendRequest(apiRequestData);
    }

    private HolidayDTO fetchNextHoliday(@NonNull final ApiRequestData apiRequestData) {
        if (apiRequestData.getState() == null) {
            return null;
        }
        return sendRequest(apiRequestData);
    }

    private HolidayDTO sendRequest(ApiRequestData apiRequestData) {
        final HttpRequest request = HttpRequest.newBuilder()
                .uri(buildUri(firetagApiConfiguration.getUrl(), apiRequestData.getDate(), apiRequestData.getState()))
                .GET()
                .build();
        try {
            final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            final String responseBody = response.body();
            if (responseBody == null) {
                throw new ApiException();
            }
            if (responseBody.equalsIgnoreCase("")) {
                return null;
            }
            return objectMapper.readValue(responseBody, HolidayDTO.class);
        } catch (IOException | InterruptedException e) {
            throw new ApiException();
        }
    }

    private URI buildUri(final String baseUrl, final LocalDate day, final State state) {
        try {
            if (day == null) {
                return new URI(new StringBuilder(baseUrl)
                        .append("?").append("state=").append(state)
                        .toString());
            } else {
                return new URI(new StringBuilder(baseUrl)
                        .append("?").append("day=").append(day)
                        .append("&").append("state=").append(state)
                        .toString());
            }

        } catch (final URISyntaxException e) {
            throw new ApiException();
        }
    }
}
