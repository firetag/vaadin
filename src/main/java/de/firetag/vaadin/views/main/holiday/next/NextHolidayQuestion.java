package de.firetag.vaadin.views.main.holiday.next;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.etc.Constants;
import de.firetag.vaadin.views.main.holiday.component.StateSelection;
import de.firetag.vaadin.views.main.holiday.model.HasMobileMode;
import de.firetag.vaadin.views.main.holiday.model.state.State;
import de.firetag.vaadin.views.main.holiday.next.component.NextStateSelection;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class NextHolidayQuestion extends HorizontalLayout implements HasMobileMode {

    private final StateSelection stateSelection;
    private final FlexLayout frageInhalt;
    private final String naechsterFeiertagPattern = "der nächste %s ?";

    @Autowired
    public NextHolidayQuestion(final NextStateSelection stateSelection) {
        this.stateSelection = stateSelection;
        frageInhalt = new FlexLayout();
        frageInhalt.setWidth(80, Unit.PERCENTAGE);
        frageInhalt.setJustifyContentMode(JustifyContentMode.CENTER);
        frageInhalt.setFlexWrap(FlexLayout.FlexWrap.WRAP);

        UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver ->
                configureLayout( receiver.getScreenWidth() <= Constants.MAX_MOBILE_WIDTH_PX));

        final H2 wannIstIn = new H2("Wann ist in ");
        wannIstIn.setSizeUndefined();

        final H2 feiertag = new H2("der nächste %s ?");
        feiertag.setSizeUndefined();

        stateSelection.addSpecificListener(event -> feiertag.setText(naechsterFeiertagPattern.formatted(event.getValue().equals(State.NONE) ? "(bundesweite) Feiertag" : "Feiertag")));

        frageInhalt.add(wannIstIn, stateSelection, feiertag);
        frageInhalt.getChildren().forEach(component -> component.getElement().getStyle().set("padding-right", "10px"));
        add(frageInhalt);

        setWidthFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
    }

    @Override
    public void configureLayout(final boolean isMobile) {
        configureLayoutByScreenWidth(isMobile, stateSelection, frageInhalt);
    }

    private void configureLayoutByScreenWidth(final boolean mobileMode,
                                              final StateSelection stateSelection,
                                              final FlexLayout frageInhalt) {
        UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver -> {
            if (mobileMode) {
                frageInhalt.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
                stateSelection.configureLayout(true);
            } else {
                frageInhalt.setFlexDirection(FlexLayout.FlexDirection.ROW);
                stateSelection.configureLayout(false);
            }
        });
    }

    public Select<State> getStateSelection() {
        return stateSelection.getStateSelect();
    }
}
