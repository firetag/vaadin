package de.firetag.vaadin.views.main.holiday.next;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.FiretagAppLayout;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.holiday.model.FiretagTabs;
import de.firetag.vaadin.views.main.holiday.model.state.State;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
@Route(value="naechster", layout = FiretagAppLayout.class)
public class NextHolidayView extends Div implements AfterNavigationObserver {

    private final NextHolidayQuestion question;
    private final NextHolidayAnswer answer;

    @Autowired
    NextHolidayView(final NextHolidayQuestion question, final NextHolidayAnswer answer) {
        this.question = question;
        this.answer = answer;

        add(question, answer);

        setWidthFull();
    }

    @Override
    public void afterNavigation(final AfterNavigationEvent afterNavigationEvent) {
        getUI().ifPresent(ui -> VaadinSession.getCurrent().getAttribute(Tabs.class).setSelectedIndex(FiretagTabs.NEXT.getTabIndex()));
        ApiRequestData requestData = VaadinSession.getCurrent().getAttribute(ApiRequestData.class);
        if (requestData == null) {
            requestData = new ApiRequestData();
        }
        requestData.setDate(null);
        VaadinSession.getCurrent().setAttribute(ApiRequestData.class, requestData);
        this.question.getStateSelection().setValue(State.NONE);
    }
}
