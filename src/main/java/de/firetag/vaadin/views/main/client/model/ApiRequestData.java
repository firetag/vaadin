package de.firetag.vaadin.views.main.client.model;

import de.firetag.vaadin.views.main.holiday.model.state.State;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ApiRequestData {
    private LocalDate date;
    private State state;

    public boolean hasDateAndState() {
        return date != null && state != null;
    }
}
