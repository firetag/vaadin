package de.firetag.vaadin.views.main.holiday.model;

public interface HasMobileMode {
    public void configureLayout(final boolean isMobile);
}
