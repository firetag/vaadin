package de.firetag.vaadin.views.main.event;

import com.vaadin.flow.spring.annotation.SpringComponent;
import de.firetag.vaadin.views.main.client.ApiClientService;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.client.model.ApiResponseData;
import de.firetag.vaadin.views.main.holiday.dated.DatedHolidayAnswer;
import de.firetag.vaadin.views.main.holiday.next.NextHolidayAnswer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Scope(value="vaadin-ui", proxyMode=ScopedProxyMode.INTERFACES)
@SpringComponent
public class ApiRequestDataChangedEventListener implements ApplicationListener<ApiRequestDataChanged> {

    private final ApiClientService apiClientService;
    private final DatedHolidayAnswer datedHolidayAnswer;
    private final NextHolidayAnswer nextHolidayAnswer;
    private final Set<ApiRequestDataChanged> processedEvents = new HashSet<>();

    @Override
    public void onApplicationEvent(final ApiRequestDataChanged event) {
        if (processedEvents.contains(event)) {
            // if this is called from DateSelection#afterNavigation (happens when navigated to the website initially),
            // the same event (!) is processed in the same listener (!) two times - producing two api requests.
            // to avoid that, we track processed events (cleared in small intervals, see #clearProcessedEvents)
            return;
        }
        final ApiRequestData apiRequestData = event.getApiRequestData();
        if (apiRequestData != null && !(apiRequestData.getDate() == null && apiRequestData.getState() == null) && apiRequestData.getState() != null) {
            final ApiResponseData apiResponseData = apiClientService.process(apiRequestData);
            displayAnswer(apiRequestData, apiResponseData);
            processedEvents.add(event);
        }
    }

    private void displayAnswer(@NonNull ApiRequestData apiRequestData, @NonNull final ApiResponseData apiResponseData) {
        if (apiRequestData.hasDateAndState()) {
            datedHolidayAnswer.buildContent(apiResponseData);
        } else {
            nextHolidayAnswer.buildContent(apiResponseData);
        }
    }

    @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)
    public void clearProcessedEvents() {
        processedEvents.clear();
    }
}