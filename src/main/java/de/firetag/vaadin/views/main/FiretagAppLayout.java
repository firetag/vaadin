package de.firetag.vaadin.views.main;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinSession;
import de.firetag.vaadin.views.main.holiday.dated.DatedHolidayView;
import de.firetag.vaadin.views.main.holiday.next.NextHolidayView;
import de.firetag.vaadin.views.main.imprint.ImprintView;

@PageTitle("Ja is denn schon wieder Feiertag?")
public class FiretagAppLayout extends AppLayout {

    public FiretagAppLayout(final DatedHolidayView feiertagView) {

        final Tabs tabs = getTabs();

        addToNavbar(true, tabs);

        setContent(feiertagView);
    }

    private Tabs getTabs() {
        final Tabs tabs = new Tabs();
        tabs.add(createTab(VaadinIcon.CALENDAR, DatedHolidayView.class),
                createTab(VaadinIcon.CALENDAR_CLOCK, NextHolidayView.class),
                createTab(VaadinIcon.ENVELOPE, ImprintView.class));
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL, TabsVariant.LUMO_EQUAL_WIDTH_TABS);
        VaadinSession.getCurrent().setAttribute(Tabs.class, tabs);
        return tabs;
    }

    private Tab createTab(VaadinIcon viewIcon, Class<? extends Div> viewClass) {
        Icon icon = viewIcon.create();
        icon.setSize("var(--lumo-icon-size-s)");
        icon.getStyle().set("margin", "auto");
        RouterLink link = new RouterLink();
        link.add(icon);
        link.setRoute(viewClass);
        link.setTabIndex(-1);

        return new Tab(link);
    }
}
