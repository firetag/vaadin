package de.firetag.vaadin.views.main.holiday.model.state;

public enum State implements Comparable<State> {

    NONE("ganz Deutschland", "images/states/de.png"),
    BY("Bayern", "images/states/by.png"),
    BW("Baden-Württemberg", "images/states/bw.png"),
    BE("Berlin", "images/states/be.png"),
    BB("Brandenburg", "images/states/bb.png"),
    HB("Bremen", "images/states/hb.png"),
    HH("Hamburg", "images/states/hh.png"),
    HE("Hessen", "images/states/he.png"),
    MV("Mecklenburg-Vorpommern", "images/states/mv.png"),
    NI("Niedersachsen", "images/states/ni.png"),
    NW("Nordrhein-Westfalen", "images/states/nw.png"),
    RP("Rheinland-Pfalz", "images/states/rp.png"),
    SL("Saarland", "images/states/sl.png"),
    SN("Sachsen", "images/states/sn.png"),
    ST("Sachsen-Anhalt", "images/states/st.png"),
    SH("Schleswig-Holstein", "images/states/sh.png"),
    TH("Thüringen", "images/states/th.png");

    private final String name;
    private final String imageSrc;

    private State(final String name, final String imageSrc) {
        this.name = name;
        this.imageSrc = imageSrc;
    }

    public String getName() {
        return name;
    }

    public String getImageSrc() {
        return imageSrc;
    }
}
