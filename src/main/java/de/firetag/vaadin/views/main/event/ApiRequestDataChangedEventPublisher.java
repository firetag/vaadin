package de.firetag.vaadin.views.main.event;

import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class ApiRequestDataChangedEventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    public ApiRequestDataChangedEventPublisher(final ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publish(final ApiRequestData apiRequestData) {
        applicationEventPublisher.publishEvent(new ApiRequestDataChanged(apiRequestData));
    }
}