package de.firetag.vaadin.views.main.holiday.dated;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.shared.Tooltip;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.etc.Constants;
import de.firetag.vaadin.views.main.client.ApiException;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.client.model.ApiResponseData;
import de.firetag.vaadin.views.main.client.model.HolidayDTO;
import de.firetag.vaadin.views.main.holiday.model.HasMobileMode;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@SpringComponent
@UIScope
public class DatedHolidayAnswer extends HorizontalLayout implements HasMobileMode {

    private VerticalLayout contentLayout = new VerticalLayout();
    private final HorizontalLayout answerWord = new HorizontalLayout();
    private final HorizontalLayout answerSentence = new HorizontalLayout();
    private final HorizontalLayout holidayName = new HorizontalLayout();
    private final HorizontalLayout holidayDetails = new HorizontalLayout();
    private Image image = null;
    private Anchor nextHolidayLink = null;
    private Boolean mobileMode;

    public DatedHolidayAnswer() {
        setWidthFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
    }

    public void buildContent(final ApiResponseData apiResponseData) {
        contentLayout.removeAll();
        final ApiRequestData apiRequestData = VaadinSession.getCurrent().getAttribute(ApiRequestData.class);
        this.removeAll();
        setupLayout(answerWord);
        setupLayout(answerSentence);
        setupLayout(holidayName);
        setupLayout(holidayDetails);
        holidayDetails.setVisible(false);
        final HolidayDTO holidayDTO = apiResponseData.getHolidayDTO();
        final ApiException apiException = apiResponseData.getApiException();
        if (apiException != null) {
            answerWord.add(new H2(new Text("¯\\_(ツ)_/¯")));
            answerSentence.add(new H3(new Text("Unsere Datenquelle für Feiertage ist gerade leider nicht erreichbar - daher keine Ahnung.")));
            image = new Image("images/offline.png", "Gezogener Stecker-Symbol, weil die Feiertage-Api nicht erreichbar ist.");
        } else if (holidayDTO == null) {
            answerWord.add(new H2(new Text("NEIN!")));
            answerSentence.add(new H3(new Text(String.format("%s ist kein Feiertag in %s", buildStringForDay(apiRequestData.getDate()), apiRequestData.getState().getName()))));
            image = new Image("images/keinFiretag.jpg", "Frau im Bett wird von Wecker geweckt.");
            nextHolidayLink = new Anchor("naechster", "Wann ist denn der nächste Feiertag?");
        } else {
            answerWord.add(new H2(new Text("JA!")));
            answerSentence.add(new H3(new Text(String.format("%s ist ein Feiertag in %s - und zwar:", buildStringForDay(holidayDTO.getDate()), apiRequestData.getState().getName()))));
            holidayName.add(new H3(new Text(holidayDTO.getName())));
            String holidayHint = holidayDTO.getHint();
            if (holidayHint != null && !holidayHint.isEmpty()) {
                final Icon vaadinIcon = new Icon(VaadinIcon.INFO_CIRCLE);
                Tooltip.forComponent(vaadinIcon)
                        .withText(holidayHint)
                        .withPosition(Tooltip.TooltipPosition.TOP_START);
                holidayName.add(vaadinIcon);
                vaadinIcon.addClickListener((ComponentEventListener<ClickEvent<Icon>>) imageClickEvent -> {
                    if (Boolean.TRUE.equals(this.mobileMode)) {
                        holidayDetails.setVisible(!holidayDetails.isVisible());
                    }
                });
                holidayDetails.add(new Text(holidayHint));
            }
            image = new Image("images/firetag.jpg", "Hund mit Partyhut schaut in die Kamera.");
        }
        image.setSizeFull();
        UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver ->
                configureLayout( receiver.getScreenWidth() <= Constants.MAX_MOBILE_WIDTH_PX));
        contentLayout.setHorizontalComponentAlignment(Alignment.CENTER);
        add(contentLayout);
    }

    private void setupLayout(final HorizontalLayout layout) {
        layout.setWidthFull();
        layout.setJustifyContentMode(JustifyContentMode.CENTER);
        layout.removeAll();
    }

    private String buildStringForDay(final LocalDate date) {
        final LocalDate today = LocalDate.now();
        if (date.isEqual(today)) {
            return String.format("Heute (%s)", date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        } else if (date.isEqual(today.plusDays(1))) {
            return String.format("Morgen (%s)", date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        } else {
            return String.format("Am %s", date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        }
    }

    @Override
    public void configureLayout(final boolean isMobile) {
        this.mobileMode = isMobile;
        if (this.mobileMode) {
            contentLayout.setWidthFull();
            contentLayout.removeAll();
            contentLayout.add(answerWord, holidayName, holidayDetails, image, nextHolidayLink);
        } else {
            contentLayout.setWidth(50, Unit.PERCENTAGE);
            contentLayout.removeAll();
            contentLayout.add(answerWord, answerSentence, holidayName, holidayDetails, image, nextHolidayLink);
        }
    }
}
