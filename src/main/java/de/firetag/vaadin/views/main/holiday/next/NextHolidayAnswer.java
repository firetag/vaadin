package de.firetag.vaadin.views.main.holiday.next;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.shared.Tooltip;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.etc.Constants;
import de.firetag.vaadin.views.main.client.ApiException;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.client.model.ApiResponseData;
import de.firetag.vaadin.views.main.client.model.HolidayDTO;
import de.firetag.vaadin.views.main.holiday.model.HasMobileMode;

import java.time.format.DateTimeFormatter;

@SpringComponent
@UIScope
public class NextHolidayAnswer extends HorizontalLayout implements HasMobileMode {

    private VerticalLayout contentLayout = new VerticalLayout();
    private final HorizontalLayout answerWord = new HorizontalLayout();
    private final HorizontalLayout answerSentence = new HorizontalLayout();
    private final HorizontalLayout holidayDate = new HorizontalLayout();
    private final HorizontalLayout holidayName = new HorizontalLayout();
    private final HorizontalLayout holidayDetails = new HorizontalLayout();
    private Image image = null;
    private Boolean mobileMode;

    public NextHolidayAnswer() {
        setWidthFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
    }

    public void buildContent(final ApiResponseData apiResponseData) {
        contentLayout.removeAll();
        final ApiRequestData apiRequestData = VaadinSession.getCurrent().getAttribute(ApiRequestData.class);
        this.removeAll();
        setupLayout(answerWord);
        setupLayout(answerSentence);
        setupLayout(holidayName);
        setupLayout(holidayDetails);
        setupLayout(holidayDate);
        holidayDetails.setVisible(false);
        final HolidayDTO holidayDTO = apiResponseData.getHolidayDTO();
        final ApiException apiException = apiResponseData.getApiException();
        if (apiException != null) {
            answerWord.add(new H2(new Text("¯\\_(ツ)_/¯")));
            answerSentence.add(new H3(new Text("Unsere Datenquelle für Feiertage ist gerade leider nicht erreichbar - daher keine Ahnung.")));
            image = new Image("images/offline.png", "Gezogener Stecker-Symbol, weil die Feiertage-Api nicht erreichbar ist.");
        } else if (holidayDTO == null) {
            answerWord.add(new H2(new Text("Dieses Jahr gibts nix mehr zu feiern!")));
            answerSentence.add(new H3(new Text(String.format("In %s gibt es dieses Jahr keinen anstehenden Feiertag mehr :(", apiRequestData.getState().getName()))));
            image = new Image("images/keinFiretag.jpg", "Frau im Bett wird von Wecker geweckt.");
        } else {
            answerWord.add(new H2(new Text(holidayDTO.getName() + "!")));
            answerSentence.add(new H3(new Text(String.format("In %s ist der nächste Feiertag am %s - und zwar:", apiRequestData.getState().getName(), holidayDTO.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))))));
            holidayDate.add(new H2(new Text(holidayDTO.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")))));
            holidayName.add(new H3(new Text(holidayDTO.getName())));
            String holidayHint = holidayDTO.getHint();
            if (holidayHint != null && !holidayHint.isEmpty()) {
                final Icon vaadinIcon = new Icon(VaadinIcon.INFO_CIRCLE);
                Tooltip.forComponent(vaadinIcon)
                        .withText(holidayHint)
                        .withPosition(Tooltip.TooltipPosition.TOP_START);
                holidayName.add(vaadinIcon);
                vaadinIcon.addClickListener((ComponentEventListener<ClickEvent<Icon>>) imageClickEvent -> {
                    if (Boolean.TRUE.equals(this.mobileMode)) {
                        holidayDetails.setVisible(!holidayDetails.isVisible());
                    }
                });
                holidayDetails.add(new Text(holidayHint));
            }
            image = new Image("images/firetag.jpg", "Hund mit Partyhut schaut in die Kamera.");
        }
        image.setSizeFull();
        UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver ->
                configureLayout( receiver.getScreenWidth() <= Constants.MAX_MOBILE_WIDTH_PX));
        contentLayout.setHorizontalComponentAlignment(Alignment.CENTER);
        add(contentLayout);
    }

    private void setupLayout(final HorizontalLayout layout) {
        layout.setWidthFull();
        layout.setJustifyContentMode(JustifyContentMode.CENTER);
        layout.removeAll();
    }

    @Override
    public void configureLayout(final boolean isMobile) {
        this.mobileMode = isMobile;
        if (this.mobileMode) {
            contentLayout.setWidthFull();
            contentLayout.removeAll();
            contentLayout.add(holidayDate, answerWord, holidayDetails, image);
        } else {
            contentLayout.setWidth(50, Unit.PERCENTAGE);
            contentLayout.removeAll();
            contentLayout.add(answerSentence, answerWord, holidayDetails, image);
        }
    }
}
