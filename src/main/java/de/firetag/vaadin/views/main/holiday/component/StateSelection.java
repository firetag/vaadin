package de.firetag.vaadin.views.main.holiday.component;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.event.ApiRequestDataChangedEventPublisher;
import de.firetag.vaadin.views.main.holiday.model.HasMobileMode;
import de.firetag.vaadin.views.main.holiday.model.state.State;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Comparator;

@SpringComponent
@UIScope
public class StateSelection extends HorizontalLayout implements HasMobileMode {
    @Getter
    protected Select<State> stateSelect = new Select<>();

    @Autowired
    public StateSelection(final ApiRequestDataChangedEventPublisher publisher) {
        final State[] states = State.values();
        Arrays.sort(states, Comparator.comparing(State::getName));
        this.stateSelect.setItems(states);
        this.stateSelect.setValue(State.NONE);
        ApiRequestData requestData = VaadinSession.getCurrent().getAttribute(ApiRequestData.class);
        if (requestData == null) {
            requestData = new ApiRequestData();
        }
        requestData.setState(this.stateSelect.getValue());
        this.stateSelect.setRenderer(createStateRenderer());
        this.stateSelect.addValueChangeListener((HasValue.ValueChangeListener<AbstractField.ComponentValueChangeEvent<Select<State>, State>>) selectStateComponentValueChangeEvent -> {
            final ApiRequestData apiRequestData = VaadinSession.getCurrent().getAttribute(ApiRequestData.class);
            if (apiRequestData != null) {
                apiRequestData.setState(stateSelect.getValue());
                VaadinSession.getCurrent().setAttribute(ApiRequestData.class, apiRequestData);
                publisher.publish(apiRequestData);
            }
        });
        add(stateSelect);
    }

    private ComponentRenderer<FlexLayout, State> createStateRenderer() {
        return new ComponentRenderer<>(state -> {
            FlexLayout wrapper = new FlexLayout();
            wrapper.setAlignItems(Alignment.CENTER);

            final Image image = new Image();
            image.setSrc(state.getImageSrc());
            image.setAlt("Flag of " + state.getName());
            image.setWidth("var(--lumo-size-m)");
            image.getStyle().set("margin-right", "var(--lumo-space-s)");

            final Div info = new Div();
            info.setText(state.getName());

            wrapper.add(image, info);
            return wrapper;
        });
    }

    @Override
    public void configureLayout(final boolean isMobile) {
        if (isMobile) {
            this.stateSelect.setWidthFull();
        } else {
            this.stateSelect.setWidth(290, Unit.PIXELS);
        }
    }

    public void addSpecificListener(HasValue.ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<Select<State>, State>> listener) {
        this.stateSelect.addValueChangeListener(listener);
    }
}
