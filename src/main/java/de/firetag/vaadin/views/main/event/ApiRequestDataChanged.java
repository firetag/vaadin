package de.firetag.vaadin.views.main.event;

import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ApiRequestDataChanged extends ApplicationEvent {

    private final ApiRequestData apiRequestData;

    public ApiRequestDataChanged(final ApiRequestData apiRequestData) {
        super(apiRequestData);
        this.apiRequestData = apiRequestData;
    }
}