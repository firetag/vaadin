package de.firetag.vaadin.views.main.holiday.dated.component;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.event.ApiRequestDataChangedEventPublisher;
import de.firetag.vaadin.views.main.holiday.component.StateSelection;

@SpringComponent
@UIScope
public class DatedStateSelection extends StateSelection {

    public DatedStateSelection(final ApiRequestDataChangedEventPublisher publisher) {
        super(publisher);
    }
}
