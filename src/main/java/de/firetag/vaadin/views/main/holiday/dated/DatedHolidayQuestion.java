package de.firetag.vaadin.views.main.holiday.dated;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.etc.Constants;
import de.firetag.vaadin.views.main.holiday.component.DateSelection;
import de.firetag.vaadin.views.main.holiday.component.StateSelection;
import de.firetag.vaadin.views.main.holiday.dated.component.DatedStateSelection;
import de.firetag.vaadin.views.main.holiday.model.HasMobileMode;
import de.firetag.vaadin.views.main.holiday.model.date.DateMode;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class DatedHolidayQuestion extends HorizontalLayout implements HasMobileMode {

    private final DateSelection dateSelection;
    private final StateSelection stateSelection;
    private final FlexLayout frageInhalt;

    @Autowired
    public DatedHolidayQuestion(final DateSelection dateSelection, final DatedStateSelection stateSelection) {
        this.dateSelection = dateSelection;
        this.stateSelection = stateSelection;
        frageInhalt = new FlexLayout();
        frageInhalt.setWidth(80, Unit.PERCENTAGE);
        frageInhalt.setJustifyContentMode(JustifyContentMode.CENTER);
        frageInhalt.setFlexWrap(FlexLayout.FlexWrap.WRAP);

        UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver ->
                configureLayout( receiver.getScreenWidth() <= Constants.MAX_MOBILE_WIDTH_PX));

        final H2 ist = new H2("Ist");
        ist.setSizeUndefined();

        final H2 in = new H2("in");
        in.setSizeUndefined();

        final H2 feiertag = new H2("Feiertag ?");
        feiertag.setSizeUndefined();

        dateSelection.setSizeUndefined();

        frageInhalt.add(ist, dateSelection, in, stateSelection, feiertag);
        frageInhalt.getChildren().forEach(component -> component.getElement().getStyle().set("padding-right", "10px"));
        add(frageInhalt);

        setWidthFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
    }

    @Override
    public void configureLayout(final boolean isMobile) {
        configureLayoutByScreenWidth(isMobile, dateSelection, stateSelection, frageInhalt);
    }

    private void configureLayoutByScreenWidth(final boolean mobileMode,
                                              final DateSelection dateSelection,
                                              final StateSelection stateSelection,
                                              final FlexLayout frageInhalt) {
        UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver -> {
            if (mobileMode) {
                frageInhalt.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
                dateSelection.configureLayout(true);
                stateSelection.configureLayout(true);
            } else {
                frageInhalt.setFlexDirection(FlexLayout.FlexDirection.ROW);
                dateSelection.configureLayout(false);
                stateSelection.configureLayout(false);
            }
        });
    }

    public Select<DateMode> getDateSelection() {
        return dateSelection.getModeSelect();
    }
}
