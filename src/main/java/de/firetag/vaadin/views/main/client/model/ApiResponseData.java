package de.firetag.vaadin.views.main.client.model;

import de.firetag.vaadin.views.main.client.ApiException;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiResponseData {
    private HolidayDTO holidayDTO;
    private ApiException apiException;
}
