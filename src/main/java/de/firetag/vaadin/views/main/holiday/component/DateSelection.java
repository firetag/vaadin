package de.firetag.vaadin.views.main.holiday.component;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.event.ApiRequestDataChangedEventPublisher;
import de.firetag.vaadin.views.main.holiday.model.HasMobileMode;
import de.firetag.vaadin.views.main.holiday.model.date.DateMode;
import de.firetag.vaadin.views.main.holiday.model.state.State;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Locale;

@SpringComponent
@UIScope
public class DateSelection extends FlexLayout implements HasMobileMode {

    @Getter
    private final Select<DateMode> modeSelect = new Select<>();
    private final DatePicker datePicker = new DatePicker();
    private Boolean isMobileMode;


    @Autowired
    public DateSelection(final ApiRequestDataChangedEventPublisher publisher) {
        this.datePicker.setLocale(new Locale("de", "DE"));
        this.modeSelect.setItems(DateMode.values());
        this.modeSelect.setTextRenderer((ItemLabelGenerator<DateMode>) dateMode -> dateMode.readable);
        this.modeSelect.addValueChangeListener((HasValue.ValueChangeListener<AbstractField.ComponentValueChangeEvent<Select<DateMode>, DateMode>>) selectDateModeComponentValueChangeEvent -> {
            switch (selectDateModeComponentValueChangeEvent.getValue()) {
                case HEUTE -> {
                    this.datePicker.setValue(LocalDate.now());
                    this.datePicker.setVisible(false);
                }
                case MORGEN -> {
                    this.datePicker.setValue(LocalDate.now().plusDays(1));
                    this.datePicker.setVisible(false);
                }
                case AM -> {
                    this.datePicker.setValue(LocalDate.now());
                    this.datePicker.setVisible(true);
                }
            }
            if (this.datePicker.isVisible()) {
                if (Boolean.TRUE.equals(isMobileMode)) {
                    this.modeSelect.getStyle().set("padding-right", "0px");
                } else {
                    this.modeSelect.getStyle().set("padding-right", "10px");
                }
            } else {
                // surrounding FlexLayout already has padding-right already
                this.modeSelect.getStyle().set("padding-right", "0px");
            }
        });

        this.datePicker.addValueChangeListener((HasValue.ValueChangeListener<AbstractField.ComponentValueChangeEvent<DatePicker, LocalDate>>) datePickerLocalDateComponentValueChangeEvent -> {
            ApiRequestData requestData = VaadinSession.getCurrent().getAttribute(ApiRequestData.class);
            if (requestData == null) {
                requestData = new ApiRequestData();
                requestData.setState(State.NONE);
            }
            requestData.setDate(datePicker.getValue());
            VaadinSession.getCurrent().setAttribute(ApiRequestData.class, requestData);
            publisher.publish(requestData);
        });
        add(this.modeSelect, this.datePicker);
        setFlexWrap(FlexLayout.FlexWrap.WRAP);
    }

    @Override
    public void configureLayout(final boolean isMobile) {
        this.isMobileMode = isMobile;
        if (this.isMobileMode) {
            this.modeSelect.setWidthFull();
            this.modeSelect.getStyle().set("padding-right", "0px");
            this.datePicker.setWidthFull();
        } else {
            this.modeSelect.setSizeUndefined();
            this.modeSelect.getStyle().set("padding-right", "10px");
            this.datePicker.setSizeUndefined();
        }
    }
}
