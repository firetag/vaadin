package de.firetag.vaadin.views.main.holiday.dated;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.FiretagAppLayout;
import de.firetag.vaadin.views.main.holiday.model.FiretagTabs;
import de.firetag.vaadin.views.main.holiday.model.date.DateMode;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
@Route(value = "", layout = FiretagAppLayout.class)
@RouteAlias(value = "morgen", layout = FiretagAppLayout.class)
public class DatedHolidayView extends Div implements AfterNavigationObserver {

    private final DatedHolidayQuestion question;
    private final DatedHolidayAnswer answer;

    @Autowired
    DatedHolidayView(final DatedHolidayQuestion question, final DatedHolidayAnswer answer) {
        this.question = question;
        this.answer = answer;

        add(question, answer);

        setWidthFull();
    }

    @Override
    public void afterNavigation(final AfterNavigationEvent afterNavigationEvent) {
        getUI().ifPresent(ui -> {
            VaadinSession.getCurrent().getAttribute(Tabs.class).setSelectedIndex(FiretagTabs.DATED.getTabIndex());
            Location location = afterNavigationEvent.getLocation();
            String url = location.getPath();
            if (url.endsWith("morgen")) {
                this.question.getDateSelection().setValue(DateMode.MORGEN);
            } else if (url.endsWith("naechster")) {
                this.question.getDateSelection().setValue(null);
            } else {
                this.question.getDateSelection().setValue(DateMode.HEUTE);
            }
        });
    }
}
