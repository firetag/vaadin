package de.firetag.vaadin.views.main.holiday.model;

import lombok.Getter;

public enum FiretagTabs {
    DATED(0),
    NEXT(1),
    IMPRINT(2);

    @Getter
    private final int tabIndex;

    FiretagTabs(int tabIndex) {
        this.tabIndex = tabIndex;
    }
}
