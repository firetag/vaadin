package de.firetag.vaadin.views.main.client;

import de.firetag.vaadin.views.main.client.model.ApiRequestData;
import de.firetag.vaadin.views.main.client.model.ApiResponseData;
import de.firetag.vaadin.views.main.client.model.HolidayDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ApiClientService {

    private final ApiClient apiClient;
    @NonNull
    public ApiResponseData process(@NonNull final ApiRequestData apiRequestData) {
        try {
            final HolidayDTO holidayDTO = processInternal(apiRequestData);
            return new ApiResponseData(holidayDTO, null);
        } catch (final ApiException apiException) {
            return new ApiResponseData(null, apiException);
        }
    }

    private HolidayDTO processInternal(@NonNull final ApiRequestData apiRequestData) {
        return apiClient.fetchHoliday(apiRequestData);
    }
}
