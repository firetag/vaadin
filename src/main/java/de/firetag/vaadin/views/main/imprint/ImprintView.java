package de.firetag.vaadin.views.main.imprint;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import de.firetag.vaadin.views.main.FiretagAppLayout;
import de.firetag.vaadin.views.main.holiday.model.FiretagTabs;

@SpringComponent
@UIScope
@Route(value="impressum", layout = FiretagAppLayout.class)
public class ImprintView extends Div implements AfterNavigationObserver {

    ImprintView() {
        final VerticalLayout imprintLayout = new VerticalLayout();
        imprintLayout.setPadding(true);
        imprintLayout.setMargin(true);
        final H2 imprintTitle = new H2("Impressum");
        final H3 tmgTitle = new H3("Angaben gemäß § 5 TMG");
        final Paragraph addressParagraph = new Paragraph("Marco Ebbinghaus\nKronprinzenstr. 53\n44135 Dortmund");
        addressParagraph.setWhiteSpace(WhiteSpace.PRE);
        final H3 contactTitle = new H3("Kontakt");
        final Paragraph contactParagraph = new Paragraph("Email: ebbinghaus.marco@gmail.com\nTelefon: +49 231 13781029");
        contactParagraph.setWhiteSpace(WhiteSpace.PRE);
        imprintLayout.add(imprintTitle, tmgTitle, addressParagraph, contactTitle, contactParagraph);

        final VerticalLayout datenschutzLayout = new VerticalLayout();
        datenschutzLayout.setPadding(true);
        datenschutzLayout.setMargin(true);
        final H2 datenschutzTitle = new H2("Datenschutzerklärung");
        datenschutzLayout.add(datenschutzTitle, new Html("<div>" +
                "<h3>Allgemeiner Hinweis und Pflichtinformationen</h3><br>\n" +
                "<h4>Benennung der verantwortlichen Stelle</h4>\n" +
                "<p>Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist:</p>\n" +
                "<span id=\"s3-t-ansprechpartner\">Marco Ebbinghaus</span><br><span id=\"s3-t-strasse\">Kronprinzenstr. 53</span><br><span id=\"s3-t-plz\">44135</span> <span id=\"s3-t-ort\">Dortmund</span></p>\n" +
                "<p>Die verantwortliche Stelle entscheidet allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten (z.B. Namen, Kontaktdaten o. Ä.).</p>\n" +
                "<h4>Widerruf Ihrer Einwilligung zur Datenverarbeitung</h4>\n" +
                "<p>Nur mit Ihrer ausdrücklichen Einwilligung sind einige Vorgänge der Datenverarbeitung möglich. Ein Widerruf Ihrer bereits erteilten Einwilligung ist jederzeit möglich. Für den Widerruf genügt eine formlose Mitteilung per E-Mail. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.</p>\n" +
                "<h4>Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde</h4>\n" +
                "<p>Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen Verstoßes ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Zuständige Aufsichtsbehörde bezüglich datenschutzrechtlicher Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz unseres Unternehmens befindet. Der folgende Link stellt eine Liste der Datenschutzbeauftragten sowie deren Kontaktdaten bereit: <a href=\"https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html\" target=\"_blank\">https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html</a>.</p>\n" +
                "<h4>Recht auf Datenübertragbarkeit</h4>\n" +
                "<p>Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an Dritte aushändigen zu lassen. Die Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.</p>\n" +
                "<h4>Recht auf Auskunft, Berichtigung, Sperrung, Löschung</h4>\n" +
                "<p>Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, Herkunft der Daten, deren Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Diesbezüglich und auch zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit über die im Impressum aufgeführten Kontaktmöglichkeiten an uns wenden.</p>\n" +
                "<h4>SSL- bzw. TLS-Verschlüsselung</h4>\n" +
                "<p>Aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, die Sie an uns als Seitenbetreiber senden, nutzt unsere Website eine SSL-bzw. TLS-Verschlüsselung. Damit sind Daten, die Sie über diese Website übermitteln, für Dritte nicht mitlesbar. Sie erkennen eine verschlüsselte Verbindung an der „https://“ Adresszeile Ihres Browsers und am Schloss-Symbol in der Browserzeile.</p>\n" +
                "\n" +
                "<h3>Server-Log-Dateien</h3>\n" +
                "<p>In Server-Log-Dateien erhebt und speichert der Provider der Website automatisch Informationen, die Ihr Browser automatisch an uns übermittelt. Dies sind:</p>\n" +
                "<ul>\n" +
                "\t<li>\n" +
                "\t\t<p>Besuchte Seite auf unserer Domain</p>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t\t<p>Datum und Uhrzeit der Serveranfrage</p>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t\t<p>Browsertyp und Browserversion</p>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t\t<p>Verwendetes Betriebssystem</p>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t\t<p>Referrer URL</p>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t\t<p>Hostname des zugreifenden Rechners</p>\n" +
                "\t</li>\n" +
                "\t<li>\n" +
                "\t\t<p>IP-Adresse</p>\n" +
                "\t</li>\n" +
                "</ul>\n" +
                "<p>Es findet keine Zusammenführung dieser Daten mit anderen Datenquellen statt. Grundlage der Datenverarbeitung bildet Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.</p>\n" +
                "\n" +
                "\n" +
                "<p><small>Quelle: Datenschutz-Konfigurator von <a href=\"https://www.mein-datenschutzbeauftragter.de\" target=\"_blank\">Mein-Datenschutzbeauftragter.de</a></small></p></div>\n"));

        final VerticalLayout techStackLayout = new VerticalLayout();
        techStackLayout.setPadding(true);
        techStackLayout.setMargin(true);
        final H2 techStackTitle = new H2("Technologie-Stack");
        techStackLayout.add(techStackTitle, new Html("""
               <div>
               <p>Diese Applikation besteht aus zwei Services.</p>
               <p>Das Backend (firetag-api) ist ein Spring Boot Service, welcher eine REST-API bereitstellt, die vom Frontend entsprechend
               der ausgewählten Such-Parameter (Datum, Bundesland/Bundesweit) angesprochen wird. Das Backend selber spricht wiederum mit der feiertage-api (weiter unten verlinkt).</p>
               <p>Das Frontend (firetag-vaadin) ist eine Spring Boot Vaadin (Flow) Applikation.</p>
               <p>Beide services hoste ich open source via Gitlab - hier: <a href="https://gitlab.com/firetag" target="_blank">Firetag Projekt</a>.</p>
               <p>Die Services werden im Rahmen der gitlab-ci pipeline als Container Images gebaut und in die Gitlab Container Registry gepusht.</p>
               <p>Betrieben werden beide Services in einem (sehr kleinen) managed Kubernetes-Cluster (weil yolo) auf <a href="https://www.digitalocean.com/" target="_blank">DigitalOcean</a> - Dem Cloud Provider meines Vetrauens.</p>
               <p>Die Domains ist-heute-feiertag.de und ist-morgen-feiertag.de habe ich über STRATO bezogen. Letztere leitet lediglich auf ist-heute-feiertag.de/morgen um. Vaadin erkennt das 'morgen'
               in der URL und setzt den Datum-Filter entsprechend auf 'Morgen'.</p>
               <p>Java-Version ist 20, Vaadin Flow-Version ist 24.</p>
               <p>Verbesserungsvorschläge und/oder Wünsche bzgl. weiterer Funktionalitäten gerne via <a href="https://gitlab.com/firetag/vaadin/-/issues/new" target="_blank">Issues</a> einreichen.</p>
               </div>
               """));

        final VerticalLayout infoLayout = new VerticalLayout();
        infoLayout.setPadding(true);
        infoLayout.setMargin(true);
        final H2 infoTitle = new H2("Informationen und Haftungsausschluss");
        infoLayout.add(infoTitle, new Html("""
               <div>
                <p>Diese Seite ist ein kleines Hobby-Projekt von mir, Marco Ebbinghaus. Ich übernehme keine Garantie für die Korrektheit der auf dieser Seite angezeigten Daten.</p>
                <p>Als Datenquelle für die Prüfung auf Feiertage verwende ich die kostenlose <a href="https://feiertage-api.de/" target="_blank">Feiertage API</a>. Viele Grüße gehen raus!</p>
               </div>
               """));

        final VerticalLayout copyrightLayout = new VerticalLayout();
        copyrightLayout.setPadding(true);
        copyrightLayout.setMargin(true);
        final H2 copyrightTitle = new H2("Informationen zu verwendeten Grafiken/Bildern");
        copyrightLayout.add(copyrightTitle, new Html("""
               <div>
                <p>Folgende Bilder werden auf dieser Seite verwendet.</p>
                <ul>
                    <li>Bundeslandflagge Baden-Württemberg: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-BW" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-BW</a></li>
                    <li>Bundeslandflagge Bayern: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-BY" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-BY</a></li>
                    <li>Bundeslandflagge Berlin: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-BE" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-BE</a></li>
                    <li>Bundeslandflagge Brandenburg: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-BB" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-BB</a></li>
                    <li>Bundeslandflagge Bremen: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-HB" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-HB</a></li>
                    <li>Bundeslandflagge Hamburg: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-HH" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-HH</a></li>
                    <li>Bundeslandflagge Hessen: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-HE" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-HE</a></li>
                    <li>Bundeslandflagge Mecklenburg-Vorpommern: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-MV" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-MV</a></li>
                    <li>Bundeslandflagge Niedersachsen: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-NI" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-NI</a></li>
                    <li>Bundeslandflagge Nordrhein-Westfalen: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-NW" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-NW</a></li>
                    <li>Bundeslandflagge Rheinland-Pfalz: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-RP" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-RP</a></li>
                    <li>Bundeslandflagge Saarland: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-SL" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-SL</a></li>
                    <li>Bundeslandflagge Sachsen: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-SN" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-SN</a></li>
                    <li>Bundeslandflagge Sachsen-Anhalt: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-ST" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-ST</a></li>
                    <li>Bundeslandflagge Schleswig-Holstein: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-SH" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-SH</a></li>
                    <li>Bundeslandflagge Thüringen: <a href="https://de.wikipedia.org/wiki/Vorlage:DE-TH" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DE-TH</a></li>
                    <li>Landesflagge Deutschland: <a href="https://de.wikipedia.org/wiki/Vorlage:DEU" target="_blank">https://de.wikipedia.org/wiki/Vorlage:DEU</a></li>
                    <li>Symbolbild Feiertag: <a href="https://www.rawpixel.com/image/5928042/photo-image-background-public-domain-pink" target="_blank">https://www.rawpixel.com/image/5928042/photo-image-background-public-domain-pink</a></li>
                    <li>Symbolbild kein Feiertag: <a href="https://www.rawpixel.com/image/388716/free-photo-image-alarm-clock-bedroom-table" target="_blank">https://www.rawpixel.com/image/388716/free-photo-image-alarm-clock-bedroom-table</a></li>
                    <li>Symbolbild Feiertage-API nicht erreichbar: <a href="https://pixabay.com/de/illustrations/stromausfall-steckdose-stecker-7557626" target="_blank">https://pixabay.com/de/illustrations/stromausfall-steckdose-stecker-7557626</a></li>
                    <li>Favicon: <a href="https://pixabay.com/de/vectors/feier-hut-geburtstag-besondere-42330/" target="_blank">https://pixabay.com/de/vectors/feier-hut-geburtstag-besondere-42330/</a></li>
                </ul>
               </div>
               """));

        add(imprintLayout, datenschutzLayout,techStackLayout, infoLayout, copyrightLayout);
        setWidth(98, Unit.PERCENTAGE);
        VaadinSession.getCurrent().getAttribute(Tabs.class).setSelectedIndex(FiretagTabs.IMPRINT.getTabIndex());
    }

    @Override
    public void afterNavigation(final AfterNavigationEvent afterNavigationEvent) {
        getUI().ifPresent(ui -> VaadinSession.getCurrent().getAttribute(Tabs.class).setSelectedIndex(FiretagTabs.IMPRINT.getTabIndex()));
    }
}
