package de.firetag.vaadin.views.main.holiday.model.date;

public enum DateMode {


    HEUTE("heute"),
    MORGEN("morgen"),
    AM("am");

    public final String readable;

    private DateMode(final String readable) {
        this.readable = readable;
    }
}
